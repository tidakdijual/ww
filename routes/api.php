<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/start', 'GameStatus@start');
Route::post('/join', 'GameStatus@join');
// Route::get('/generateRole', 'GameStatus@generateRole');

Route::post('/vote', 'GameProgress@vote');
Route::get('/checkGame', 'GameProgress@checkGame');


