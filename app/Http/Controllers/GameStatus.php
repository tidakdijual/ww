<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameStatus extends Controller
{
    //
    public function start(Request $request){
    	$active = DB::table('group')->whereIn('status', [0, 1])->count();
    	if($active > 0){
    		return "game sedang berjalan";
    	}
    	$id = DB::table('group')->insertGetId([
    		'nama'	=> $request->get('nama')
    	]);

        $message[] = ['to' => 'all', 'title' => $request->get('nama'), 'message' => 'GAME WEREWOLF BOT. untuk join PM *JOIN '.$id.'* ke GM'];

    	return json_encode($message);
    }

    public function join(Request $request, $gid = 0, $phone = 0, $nama = 0, $title = 0){
        $gid = $request->get('gid');
        $phone = $request->get('phone');
        $title = $request->get('title');
        $nama = $request->get('nama');

        $message = [];

    	$gp = DB::table('group')->where('id', $gid)->where('status', 0)->first();
    	if(is_null($gp)){
    		$message[] = ['to' => $phone, 'title' => $title, 'message' => 'Group Tidak Ada / Salah'];
            return json_encode($message);
    	}

    	$count = DB::table('player')->where('gid', $gid)->count();
    	if($count > 15){
            $message[] = ['to' => $phone, 'title' => $title, 'message' => 'Jumlah Player Melebihi kapasitas'];
    		return json_encode($message);
    	}

        $cekNama = DB::table('player')->where('phone', $phone)->first();
        if(!is_null($cekNama)){
            if($nama == 'online'){
                $nama = $cekNama->nama;                
            }

        }

        DB::table('player')->updateOrInsert(['gid'   => $gid, 'phone' => $phone], [
            'title' => $title,
            'nama'  => $nama
        ]);


        $message[] = ['to' => $phone, 'title' => $title, 'message' => 'berhasil begabung'];
        $message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => $nama.' begabung dalam game'];
        return json_encode($message);
    	// return "berhasil begabung dengan game $gid";
    }


    
}
