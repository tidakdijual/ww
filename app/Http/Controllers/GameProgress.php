<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameProgress extends Controller
{
    //
    public function checkGame(Request $request){
    	$gp = DB::table('group')->whereIn('status', [0, 1])->first();
    	$gid = $gp->id;

    	
    	$message = [];
    	if($gp->notif < 10){
    		DB::table('group')->where('id', $gp->id)->update(['notif' => $gp->notif + 2]);	
	    	if($gp->status == '0'){
	    		//masih pendaftaran
	    		$peserta = $this->cekPeserta($request, $gp->id);
	    		$message[] = ['to' => 'all', 'title' => $gp->nama, 
	    			'message' => 'WereWolf Game Project. \n Untuk join silahkan *JOIN '.$gp->id.'* ke GM.'];
	    		$message[] = $peserta;
	    	}else{
	    		//sedang berjalan
	    		if($gp->step %  2 == 0){
	    			//siang
		    		$message[] = ['to' => 'all', 'title' => $gp->nama, 
		    			'message' => 'Sesi SIANG. \n silahkan voting dengan mengirimkan *VOTE X* (X no peserta) ke GM.'];	    		
		    		
	    		}else{
	    			//malam
	    			$message[] = ['to' => 'all', 'title' => $gp->nama, 
		    			'message' => 'Sesi MALAM. \n silahkan para pekerja malam untuk mengirimkan *VOTE X* (X no peserta) ke GM.'];
		    		DB::table('group')->where('id', $gp->id)->update(['notif' => $gp->notif + 4]);
	    		}

	    	} 

    	}else{
    		if($gp->status == 0){
    			$pesan = $this->generateRole($request, $gp->id);
    			if(is_array($pesan)){
    				$message = array_merge($message, $pesan);
    			}
    		}else{
    			$message[] = ['to' => 'all',  'title' => $gp->nama, 'message' => 'Sesi Selesai. bot ngitung matematika dulu, jangan buru2 vote.'];
				if($gp->skip == $gp->step){
					$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Karena pengaruh SAND MAN, sesi malam ditiadakan'];
				}else{
					$pesan = $this->prosesLog($request, $gid);
					$message = array_merge($message, $pesan);
				}

				if($gp->step %  2 == 0){
					$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Sesi Siang selesai, silahkan pekerja malam untuk vote'];
				}else{
					$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Sesi Malam selesai, silahkan vote siapa yang akan di lynch'];	
				}
				
    		}

    		$message[] = $this->cekPeserta($request, $gp->id);
    		DB::table('group')->where('id', $gp->id)->update(['notif' => 0, 'step' => $gp->step + 1]);
    	}


    	$countWW = DB::table('player')->where('gid', $gid)->where('alive', 1)->whereIn('kdrole', ['LC', 'WW', 'SC', 'LW'])->count();
    	$countNetral = DB::table('player')->where('gid', $gid)->where('alive', 1)->whereIn('kdrole', ['AR', 'SK'])->count();
    	$countVi = DB::table('player')->where('gid', $gid)->where('alive', 1)->whereNotIn('kdrole', ['LC', 'WW', 'SC', 'LW', 'AR', 'SK'])->count();


    	if($gp->status == 1){
    		if($countVi < $countWW + $countNetral){
    			if($countWW == 0 or $countNetral == 0){
    	    		if($countWW == 0){
		    			$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Game Selesai, Arsonist / Serial Killer WIN'];
		    		}elseif($countNetral == 0){
						$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Game Selesai, WereWolf WIN'];
		    		}
		    		DB::table('group')->where('id', $gp->id)->update(['status' => 9]);
    			}

	    		

	    	}elseif($countWW == 0 and $countNetral == 0){
	    		$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Game Selesai, Villager WIN'];	
	    		DB::table('group')->where('id', $gp->id)->update(['status' => 9]);
	    	}elseif($countWW == 0 and $countNetral == 0 and $countVi == 0){
	    		$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Game Selesai, DRAW'];
				DB::table('group')->where('id', $gp->id)->update(['status' => 9]);
	    	}
    	}
    	


    	return json_encode($message);

    }

    public function rekapVoting(Request $request, $gid = 0, $step =0){

    }

    public function cekPeserta(Request $request, $gid = 0){
    	$gp = DB::table('group')->where('id', $gid)->first();
    	$players = DB::table('player')->where('gid', $gid)->orderBy('unik', 'asc')->get();
    	$text = 'Daftar Peserta: \n';

    	foreach ($players as $player) {
    		# code...
    		if($player->alive == 1){
    			//masih hidup
    			if($gp->status == '0'){
    				$text .= $player->nama.' ('.$player->phone.'); \n'; 
    			}else{
	    			$text .= $player->unik.'.  '.$player->nama.' ('.$player->phone.') - ALIVE; \n ';    				
    			}
    		}else{
    			$role = DB::table('role')->where('kdrole', $player->kdrole)->first();
    			//mati
    			if($gp->status == '0'){
    				$text .= $player->nama.' ('.$player->phone.'); \n'; 
    			}else{
    				if($player->kdrole == 'FO'){
    					$role->nama = 'FOOL';
    				}
    				$text .= '~'.$player->unik.'. '.$player->nama.' ('.$player->phone.') / '.$role->nama.'~ - DEAD; \n';
    			}
    			
    		}
    	}

    	$text .= '-----';

    	$message = ['to' => 'all', 'title' => $gp->nama, 'message' => $text];
    	return $message;

    }


    public function generateRole(Request $request, $gid = 0){
    	$gp = DB::table('group')->where('status', 0)->where('id', $gid)->first();
    	$kembali = [];
    	if(is_null($gp)){
    		return "group tidak ada / sedang berjalan / telah selesai";
    	}
        $gid = $gp->id;

    	$count = DB::table('player')->where('gid', $gid)->count();

    	if($count < 6){
    		DB::table('group')->where('id', $gid)->where('status', 0)->update(['status' => '9']);
    		$kembali[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'player tidak mencukupi'];
    		return $kembali;
    	}

    	$jenis 	= DB::table('type')->where('player', $count)->first();
        // dd($jenis);
        // DB::table('group')->where('id', $gp->id)->update([]);

    	$arr 	= explode(" ", $jenis->format);


    	shuffle($arr);


    	$players = DB::table('player')->where('gid', $gid)->get();

    	$no = 0;

    	
    	$kembali[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Pembagian Role Sedang berlangsung, mohon bersabar.'];

    	foreach ($players as $player) {
    		# code...
    		$role = DB::table('role')->where('kdrole', $arr[$no++])->first();
    		DB::table('player')->where('id', $player->id)->update([
    			'kdrole'	=> $role->kdrole,
    			'night'		=> $role->night,
    			'first_night'	=> $role->first_night,
    			'chance'	=> $role->chance,
    			'team'		=> $role->team,
    			'delta'		=> $role->delta,
    			'save'		=> $role->save,
    			'alive'		=> 1,
                'unik'      => $no
    		]);
    		$kembali[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Role anda adalah: '.$role->nama.'. Deskripsi: '.$role->keterangan];
    	}

    	$masons 	= DB::table('player')->where('kdrole', 'MA')->where('gid', $gid)->get();
    	if(count($masons) > 1){
    		$pesanM = 'Anggota Mason adalah: ';
    		foreach ($masons as $mason) {
    			# code...
    			$pesanM 	.= $mason->unik.'. '.$mason->nama.'; ';
    		}

    		foreach ($masons as $mason) {
    			# code...
    			$kembali[] = ['to' => $mason->phone, 'title' => $mason->title, 'message' => $pesanM];
    		}
    	}

    	$weres 	= DB::table('player')->whereIn('kdrole', ['WW', 'LC', 'SC'])->where('gid', $gid)->get();
    	if(count($weres) > 1){
    		$pesanW = 'Anggota WereWolf adalah: ';
    		foreach ($weres as $were) {
    			# code...
    			$pesanW 	.= $were->unik.'. '.$were->nama.' ('.$were->kdrole.'); ';
    		}

    		foreach ($weres as $were) {
    			# code...
    			$kembali[] = ['to' => $were->phone, 'title' => $were->title, 'message' => $pesanW];
    		}
    	}

    	DB::table('group')->where('id', $gid)->where('status', 0)->update(['status' => '1', 'tid' => $jenis->id]);
    	$kembali[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Pembagian Role Selesai. Mulai Sesi Malam'];
    	return $kembali;

    }

	public function vote(Request $request, $phone = 0, $vote = 0){
		$phone = $request->get('phone');
		$vote = $request->get('vote');
		$user = DB::table('player')
				->leftJoin('group', 'player.gid', '=', 'group.id')
				->select(DB::raw('player.*, group.status, group.step'))
				->where('group.status', 1)
				->where('player.phone', $phone)
				->where('player.alive', 1)
				->first();
		$gp = DB::table('group')->where('id', $user->gid)->first();
		$target = DB::table('player')->where('gid', $gp->id)->where('unik', $vote)->where('alive', '1')->first();
		$message = [];
		//belum cek yg skip user	
		if(!is_null($user) && !is_null($target)){
			$message[] = ['to' => $user->phone, 'title' => $user->title, 'message' => 'Pilihan '.$vote.' diterima '];
			if($user->step % 2 == 0){
				//siang
				if($user->unik != $vote){
					DB::table('log')->updateOrInsert(['gid' => $user->gid, 'pid' => $user->id, 'step' => $user->step], [
			        	'tpid'  		=> $vote
			        ]);
			        $message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'User '.$user->unik.'. '.$user->nama.' memilih '.$target->unik.'. '.$target->nama];
				}

		        if($user->kdrole == 'SN' and $user->unik == $vote){
		        	DB::table('group')->where('id', $user->gid)->updat(['skip' => $user->step + 1]);	        		
		        	DB::table('player')->where('id', $user->id)->updat(['chance' => 0]);
	        	}
			}else{
				if($user->skip != $user->step){
					if($user->night == 1 and $user->unik != $vote){
						DB::table('log')->updateOrInsert(['gid' => $user->gid, 'pid' => $user->id, 'step' => $user->step], [
				        	'tpid'  		=> $vote
				        ]);
					}

					if($user->step == 1 and $user->first_night == 1){
						DB::table('log')->updateOrInsert(['gid' => $user->gid, 'pid' => $user->id, 'step' => $user->step], [
				        	'tpid'  		=> $vote
				        ]);
					}

				}
			}
			

		}else{
			$message[] = ['to' => $user->phone, 'title' => $user->title, 'message' => 'Pilihan '.$vote.' tidak valid '];
		}	


		return json_encode($message);
	}


	public function gantiWaktu(Request $request){
		$gp 	= DB::table('group')->where('status', 1)->first();
		$step 	= $gp->step + 1;
		$opening = [];
		$opening[] = ['to' => 'all',  'title' => $gp->nama, 'message' => 'Malam / Siang menjelang. bot ngitung2 dulu, jangan buru2 vote.'];
		if($gp->skip == $step){
			$step += 1;
			$opening[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Karena pengaruh SAND MAN, sesi malam ditiadakan'];
		}else{
			$pesan = $this->prosesLog($request);
			$opening = array_merge($opening, (array)json_decode($pesan));
		}
		DB::table('group')->where('id', $gp->id)->update(['step' => $step]);
		$opening[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Sesi ini selesai, silahkan vote'];
		return json_encode($opening);
	}

	public function prosesLog(Request $request, $gid = 0){
		$gp 	= DB::table('group')->where('status', 1)->where('id', $gid)->first();
		
		$step 	= $gp->step;

		$message = [];
		$all 	= '';

		$gaTarget = 0;

		if($step % 2 == 0){
			//siang
			//perhitungan voting lynch
			$sql 	= 'SELECT a.tpid, count(*) as jml 
						FROM log a
						LEFT JOIN player b on a.pid = b.id
						LEFT JOIN player c on a.tpid = c.unik and a.gid = c.gid
						where a.gid = ? 
							and step = ?
							and b.alive = 1
							and c.alive = 1
						GROUP BY a.tpid
						';
			$querys  = DB::table(DB::raw("($sql) a"))
        			->setBindings([$gp->id, $step])
                    ->selectRaw('a.*')
                    ->orderBy('jml', 'desc')
                    ->get();



            $no = 0;
            $die = 0;
            $selects = 'SELECT b.unik, b.nama, c.nama as namas, c.unik as uniks 
            			from log a 
            			left join player b on a.pid = b.id
            			left join player c on a.tpid = c.unik and a.gid = c.gid
            			where a.gid = ? 
							and step = ?
							and b.alive = 1
							and c.alive = 1
            ';

            $selects  = DB::table(DB::raw("($selects) a"))
        			->setBindings([$gp->id, $step])
                    ->selectRaw('a.*')
                    ->get();


            foreach ($querys as $vote) {
            	# code...
            	if($no == 0){
            		$no = $vote->jml;
            		$die = $vote->tpid;
            	}else{
	            	if ($no == $vote->jml) {
	            		# code...
	            		$die = 0;
	            	}  
	            	break;         		
            	}
            }
            // dd($querys);
            // dd($die);

            $pesan = '';
            foreach ($selects as $key) {
            	# code...
            	$pesan .= 'Player '.$key->unik.'. '.$key->nama.' memilih '.$key->uniks.'. '.$key->namas.' \n';
            }

            $votings = 'Rekap Voting Siang ('.$step.'):\n '.$pesan.' kesimpulan: \n';
            $voting = '';
            foreach ($querys as $vote) {
            	# code...
            	$voting .= $vote->tpid.' = '.$vote->jml.' vote; \n';
            }
            $voting = $votings.$voting;

            if($die == 0){

            	// $message[] = ['to' => 'all', 'message' => 'Siang ini tidak ada yang mati.'];

            	$message[] = ['to' => 'all',  'title' => $gp->nama, 'message' => $votings.'Siang ini tidak ada yang mati.'];
            }else{
            	//if prince
            	$getLynch = DB::table('player')->where('gid', $gp->id)->where('unik', $die)->first();
            	if($getLynch->kdrole == 'PR' and $getLynch->chance > 0){
            		DB::table('player')->where('gid', $gp->id)->where('unik', $die)->update(['chance' => '0']);
            		$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Sang Prince alias '.$die.' menunjukkan dirinya, dia berhasil lolos untuk kali ini dari tiang gantung. Siang ini tidak ada yang mati. '];
            	}else{
            		$roleLynch = DB::table('role')->where('kdrole', $getLynch->kdrole)->first();

            		$textA = '';
            		$roleAL 	= DB::table('role')->where('kdrole', $getLynch->kdrolea)->first();
            		if(!is_null($roleAL)){
            			$textA .= '('.$roleAL->nama.')';
            		}
            		DB::table('player')->where('gid', $gp->id)->where('unik', $die)->update(['alive' => '0', 'stepdie' => $step]);
            		$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => $voting.'Rakyat memilih '.$die.' sang '.$roleLynch->nama.' '.$textA.' untuk gantung. '];

            	}
            }


		}else{
			//malam

			//first night player only
			if($step == 1){
				$players = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('first_night', 1)->where('skip', '<>', $step)->orderBy('delta', 'asc')->get();
				foreach ($players as $player) {
					# code...
					//DG LV
					$log = DB::table('log')->where('gid', $gp->id)->where('pid', $player->id)->where('step', $step)->first();
					if($player->kdrole == 'DG'){
						DB::table('player')->where('id', $player->id)->update(['partner' => $log->tpid]);
						$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Jika '.$log->tpid.' mati, maka rolenya akan menjadi milik anda.'];
					}

					if($player->kdrole == 'LV'){
						DB::table('player')->where('id', $player->id)->update(['partner' => $log->tpid]);
						$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
						$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Anda dan '.$log->tpid.' menjadi pasangan, hidup dan mati bersama.'];
						$message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda dan '.$player->unik.' menjadi pasangan, hidup dan mati bersama.'];
					}
				}
			}


			//get alive player with night ability sort by delta
			$players = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('night', 1)->where('skip', '<>', $step)->orderBy('delta', 'asc')->get();
			foreach ($players as $player) {
				# code...
				$log = DB::table('log')->where('gid', $gp->id)->where('pid', $player->id)->where('step', $step)->first();
				if(!is_null($log)){

					//delta 1
					if($player->kdrole == 'SN'){
						DB::table('group')->where('status', 1)->update(['step' => $step]);
						$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Sand Man menidurkan pekerja malam'];
						return $message;
					}


					//delta 10
					// if($player->kdrole == 'DG'){
					// 	if($step == 1){
					// 		DB::table('player')->where('id', $id)->update(['partner' => $log->tpid]);
					// 		$message[] = ['to' => $player->phone, 'message' => 'Jika '.$log->tpid.' mati, maka role dia akan menjadi milik anda'];
					// 	}
					// }
					if($player->kdrole == 'FO'){	
						$sisa 	= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->whereNotIn('kdrole', ['SR', 'FO'])->get();
						$random = rand(0, count($sisa) - 1);
						// dd($sisa);
						$muncul = $sisa[$random];
						// dd($muncul);
						$role 	= DB::table('role')->where('kdrole', $muncul->kdrole)->first();

						$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Player '.$log->tpid.' adalah seorang '.$role->nama];
					}
					if($player->kdrole == 'GA'){
						// DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->update([ 'save' => DB::raw('save + 100')]);
						$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Anda melindungi '.$log->tpid];
						$gaTarget = $log->tpid;
					}	
					if($player->kdrole == 'HA'){
						$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
						if(!is_null($target)){
							if(in_array($target->kdrole, ['AR', 'SK', 'WW', 'LW', 'LC', 'SC'])){
								DB::table('player')->where('id', $player->id)->update(['alive' => 0, 'stepdie' => $step]);
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Anda dibunuh oleh '.$log->tpid.' karena dia menolak di anu-anuin.'];
								$message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Si '.$log->pid.' mencoba untuk meng-anu-anu kamu'];
							}else{
								DB::table('player')->where('id', $player->id)->where('alive', 1)->update([ 'save' => '1']);
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Anda anu-anu sama '.$log->tpid];
								$message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda di anu-anu sama '.$log->pid];	
							}
														
						}

					}

					if($player->kdrole == 'OR'){
						$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
						if(!is_null($target)){
							if(in_array($target->kdrole, ['WW', 'LW', 'LC', 'SC'])){
								$message[] = ['to' => $player->phone,  'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Wolf'];
							}else{
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' bukanlah Wolf'];
							}
														
						}

					}

					if($player->kdrole == 'SR'){
						$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
						if(!is_null($target)){
							if(in_array($target->kdrole, ['LC'])){
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Villager'];
							}elseif(in_array($target->kdrole, ['FO'])){
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Fool'];
							}else{
								$role = DB::table('role')->where('kdrole', $target->kdrole)->first();
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah '.$role->nama];
							}
														
						}

					}


					//killing progres delta 100
					if(in_array($player->kdrole, ['SC', 'LW'])){
						$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
						if($player->kdrole == 'SC'){
							if($target->kdrole == 'SR'){
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Seer'];
							}else if($target->kdrole == 'OR'){
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Oracle'];
							}else{
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' bukanlah Seer'];
							}
						}

						if($player->kdrole == 'LW'){
							if(in_array($target->kdrole, ['WW', 'LW', 'LC', 'SC'])){
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' adalah Wolf'];
							}else{
								$message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'User '.$log->tpid.' bukanlah Wolf'];
							}
						}
					}


                    // dd($log->tpid);
					$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
                    // dd($log->tpid);
					$pelaku = DB::table('role')->where('kdrole', $player->kdrole)->first();
					$korban = DB::table('role')->where('kdrole', $target->kdrole)->first();
					$korbana = '';
					if($target->kdrolea != '0'){
						$roleasli = DB::table('role')->where('kdrole', $target->kdrolea)->first();
						$korbana = '('.$roleasli->nama.')';
					}

					if($target->kdrole == "FO"){
						$korban->nama = 'FOOL';
					}

					if($player->kdrole == 'SK' or $player->kdrole == 'AR' or $player->kdrole == 'GN'){
						if($player->kdrole == 'GN'){
							if($player->chance <= 0){
								continue;
							}else{
								DB::table('player')->where('id', $player->id)->update([ 'chance' => DB::raw('save - 1')]);
							}
						} 

						if($target->save > 0 or $gaTarget == $log->tpid){
							
							if($gaTarget != $log->tpid){
								DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->update([ 'save' => DB::raw('save - 1')]);
								$message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda gagal dibunuh oleh '.$pelaku->nama];
							}

						}else{
							DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->update([ 'alive' => 0, 'stepdie' => $step ]);
							// $message[] = ['to' => $player->phone, 'title' => $player->title, 'message' => 'Anda berhasil membunuh '.$log->tpid];
							// $message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda telah dibunuh oleh '.$pelaku->nama];

							if($target->kdrole == 'DR'){
								DB::table('player')->where('id', $player->id)->update(['skip' => $step + 2]);
							}

							if($player->kdrole == 'GN'){
								$pelaku->nama = $player->id.' sebagai '.$pelaku->nama;
							}

							$all .= 'Player '.$log->tpid.' as '.$korban->nama.$korbana.' telah dibunuh oleh '.$pelaku->nama.'. \n';

							if($target->kdrole == 'HN'){
								if (rand(1,100)<='40'){
									DB::table('player')->where('id', $player->id)->update(['alive' => 0]);
									$all .= 'Hunter '.$log->tpid.' as '.$korban->nama.$korbana.' tidak mau mati sendirian, dia menembak '.$player->unik.' yang menurapakan seorang '.$pelaku->nama.'. \n';
								}
							}
						}
					}

				}
			}



			//wolf killing

			$wolfTargets = DB::table('log')
							->leftJoin('player', 'log.pid', '=', 'player.id')
							->select(DB::raw('log.*'))
							->where('log.gid', $gp->id)
							->whereIn('player.kdrole', ['WW', 'LC'])
							->where('player.skip', '<>', $step)
							->where('player.alive', 1)
							->where('log.step', $step)->get();
							
			if($wolfTargets->count()>0){
				// dd($wolfTargets);
				$random = rand(0, count($wolfTargets) - 1);
				$log 	= $wolfTargets[$random];

				$target = DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->first();
				
				$korban = DB::table('role')->where('kdrole', $target->kdrole)->first();
				$korbana = '';
				if($target->kdrolea != '0'){
					$roleasli = DB::table('role')->where('kdrole', $target->kdrolea)->first();
					$korbana = '('.$roleasli->nama.')';
				}
				if($target->kdrole == "FO"){
					$korban->nama = 'FOOL';
				}

				$wolfs = DB::table('player')->whereIn('kdrole', ['WW', 'LC'])->where('player.skip', '<>', $step)->where('player.alive', 1)->get();

				if($target->save > 0 or $gaTarget == $log->tpid){

					if($gaTarget != $log->tpid){
						DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->update([ 'save' => DB::raw('save - 1')]);
						$message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda gagal dibunuh oleh WereWolf'];
					}



				}else{
					DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('unik', $log->tpid)->update([ 'alive' => 0, 'stepdie' => $step ]);
					// $message[] = ['to' => $target->phone, 'title' => $target->title, 'message' => 'Anda telah dibunuh oleh WereWolf'];

					if($target->kdrole == 'DR'){
						DB::table('player')->where('gid', $gp->id)->where('alive', 1)->whereIn('kdrole', ['WW', 'LC'])->update(['skip' => $step + 2]);
					}

					$all .= 'Player '.$log->tpid.' as '.$korban->nama.$korbana.' telah dimangsa oleh WereWolf. \n';

					if($target->kdrole == 'HN'){
						if (rand(1,100)<='40'){
							$random = rand(0, count($wolfs) - 1);
							// $random = array_rand($wolfs);
							$player = $wolfs[$random];
							DB::table('player')->where('id', $player->id)->update(['alive' => 0]);
							$pelaku = DB::table('role')->where('kdrole', $player->kdrole)->first();

							$all .= 'Hunter '.$log->tpid.' as '.$korban->nama.$korbana.' tidak mau mati sendirian, dia menembak '.$player->unik.' yang menurapakan seorang '.$pelaku->nama.'. \n';
						}
					}
				}

				
			}

			$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Rekap malam: 
				'.$all];
			// $random = array_rand($wolfTargets);

			


			//restore effect GA, wolf count, traitor, doppleganger, lover dll, skip dll

			//GA 
			// DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('save', '>', '10')->update(['']);
			

			//DG //LV

		}


		$mainWolf 	= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->whereIn('kdrole', ['LC', 'WW'])->count();
		$mainSC		= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('kdrole', 'SC')->first();
		$mainLW		= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('kdrole', 'LW')->first();
		$mainTR		= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('kdrole', 'TR')->first();

		if($mainWolf < 1){
			if(!is_null($mainSC)){
				DB::table('player')->where('id', $mainSC->id)->update(['kdrole' => 'WW', 'kdrolea' => 'SC']);
				$message[] = ['to' => $mainSC->phone,  'title' => $mainSC->title, 'message' => 'Anda berubah jadi WereWolf'];
			}else if(!is_null($mainLW)){
				DB::table('player')->where('id', $mainLW->id)->update(['kdrole' => 'WW', 'kdrolea' => 'LW']);
				$message[] = ['to' => $mainLW->phone, 'title' => $mainLW->title, 'message' => 'Anda berubah jadi WereWolf'];
			}else if(!is_null($mainTR)){
				DB::table('player')->where('id', $mainTR->id)->update(['kdrole' => 'WW', 'kdrolea' => 'LW', 'team' => 'W']);
				$message[] = ['to' => $mainTR->phone, 'title' => $mainTR->title, 'message' => 'Anda berubah jadi WereWolf'];
			}
		}


		$dg 	= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('kdrole', 'DG')->first();
		$lv 	= DB::table('player')->where('gid', $gp->id)->where('alive', 1)->where('kdrole', 'LV')->first();
		

		if($step == 1){
			if(!is_null($dg)){
				$logDg 	= DB::table('log')->where('gid',  $gp->id)->where('pid', $dg->id)->where('step', 1)->first();
				if(is_null($logDg)){
					$alive 	= DB::table('player')->where('alive', 1)->where('id', '<>', $dg->id)->get();
					$random = rand(0, count($alive) - 1);
					// $random = array_rand($alive);
					$tDg	= $alive[$random];
					DB::table('player')->where('id', $dg->id)->update(['partner' => $tDg->unik]);
					$dg->partner = $tDg->unik;
				}
			}

			if(!is_null($lv)){
				$logLv 	= DB::table('log')->where('gid',  $gp->id)->where('pid', $lv->id)->where('step', 1)->first();
				if(is_null($logLv)){
					$alive 	= DB::table('player')->where('alive', 1)->where('id', '<>', $lv->id)->get();
					$random = rand(0, count($alive) - 1);
					// $random = array_rand($alive);
					$tLv	= $alive[$random];
					DB::table('player')->where('id', $lv->id)->update(['partner' => $tLv->unik]);
					$lv->partner = $tLv->unik;
				}

			}			
		}

		if(!is_null($dg)){
			$targetDg = DB::table('player')->where('gid', $gid)->where('unik', $dg->partner)->where('alive', '0')->where('stepdie', $step)->first();
			if(!is_null($targetDg)){
				$role 	= DB::table('role')->where('kdrole', $targetDg->kdrole)->first();
				DB::table('player')->where('id', $dg->id)->update(['kdrole' => $targetDg->kdrole, 'kdrolea' => 'DG']);
				$message[] = ['to' => $dg->phone, 'title' => $dg->title, 'message' => 'Anda berubah menjadi '.$role->nama];
			}
		}

		if(!is_null($lv)){
			$targetLv = DB::table('player')->where('gid', $gid)->where('unik', $lv->partner)->where('alive', '0')->where('stepdie', $step)->first();
			if(!is_null($targetLv)){
				DB::table('player')->where('id', $lv->id)->update(['alive' => 0, 'stepdie' => $step]);
				$roleTg = DB::table('role')->where('kdrole', $targetLv->kdrole)->first();
				$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Karena '.$lv->partner.' mati, maka '.$lv->unik.' (si Lover) bunuh diri menyusulnya.'];
			}

			if($lv->stepdie == $step){
				DB::table('player')->where('gid', $gid)->where('unik', $lv->partner)->update(['alive' => 0, 'stepdie' => $step]);
				$targetLv = DB::table('player')->where('gid', $gid)->where('unik', $lv->partner)->first();
				$roleTg = DB::table('role')->where('kdrole', $targetLv->kdrole)->first();
				if($roleTg->kdrole == 'FO'){
					$roleTg->nama = 'Fool';
				}
				$message[] = ['to' => 'all', 'title' => $gp->nama, 'message' => 'Karena '.$lv->unik.' mati, maka '.$lv->partner.' si '.$roleTg->nama.'  bunuh diri menyusulnya.'];
			}
		}


		// DB::table('group')->where('id', $gp->id)->update(['step' => $step]);

		// dd($message);
		return $message;
		
		//DG //LV
		
	}





}
