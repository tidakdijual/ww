<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Group extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('type', function (Blueprint $table) {
            $table->id();
            $table->string('kdtype')->default(0);
            $table->integer('player')->default(0);
            $table->text('format')->default(0);
            $table->timestamps();
        });

        Schema::create('group', function (Blueprint $table) {
            $table->id();
            $table->integer('step')->default(0);
            $table->integer('status')->default(0);
            $table->integer('tid')->default(0);
            $table->string('nama')->default(0);
            $table->integer('skip')->default(0);
            $table->integer('notif')->default(0);
            $table->timestamps();
        });

        Schema::create('role', function (Blueprint $table) {
            $table->id();
            $table->string('kdrole')->default(0)->unique;
            $table->string('nama')->default(0);
            $table->text('keterangan')->default(0);
            $table->string('night')->default('0');
            $table->integer('chance')->default(0);
            $table->string('first_night')->default('0');
            $table->string('team')->default('0');
            $table->integer('delta')->default('0');
            $table->integer('save')->default('0');
            $table->timestamps();
        });

        Schema::create('player', function (Blueprint $table) {
            $table->id();
            $table->integer('unik')->default('0');
            $table->integer('gid')->default(0);
            $table->string('phone')->default('0');
            $table->string('nama')->default('0');
            $table->string('title')->default('0');
            $table->integer('alive')->default(1);
            $table->integer('stepdie')->default(0);
            $table->string('kdrole')->default('0');
            $table->string('team')->default('0');
            $table->string('night')->default('0');
            $table->string('temp_night')->default('0');
            $table->string('first_night')->default('0');
            $table->integer('chance')->default(0);
            $table->integer('skip')->default(0);
            $table->integer('save')->default(0);
            $table->integer('delta')->default('0');
            $table->integer('partner')->default('0');
            $table->string('kdrolea')->default('0');
            // $table->string('tgkdrole')->default('0');
            $table->timestamps();
        });


        Schema::create('log', function (Blueprint $table) {
            $table->id();
            $table->integer('gid')->default(0);
            $table->integer('pid')->default(0);
            $table->integer('step')->default(0);
            $table->integer('tpid')->default(0);
            $table->text('return')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
