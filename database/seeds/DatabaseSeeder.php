<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        //kdtype
    	//GAME TYPE

        // AVAILABLE: DR DG FO SR VL LV MA GA HN TR WW LC SC LW SK

    	DB::table('type')->updateOrInsert(['kdtype' => 'P6M1'], [
        	'player'  		=> '6', 
        	'format'     	=> 'FO SR VL VL VL WW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P6M2'], [
        	'player'  		=> '6', 
        	'format'     	=> 'SR LV VL VL VL WW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P6M3'], [
        	'player'  		=> '6', 
        	'format'     	=> 'SR GA LV VL LC WW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P6M4'], [
            'player'        => '6', 
            'format'        => 'SR GA LV TR WW VL'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P7M1'], [
        	'player'  		=> '7', 
        	'format'     	=> 'SR GA FO VL VL LC SK'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P7M2'], [
        	'player'  		=> '7', 
        	'format'     	=> 'SR GA VL VL VL LC LW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P7M3'], [
        	'player'  		=> '7', 
        	'format'     	=> 'SR FO VL VL VL WW DG'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P7M4'], [
        	'player'  		=> '7', 
        	'format'     	=> 'SR GA LV VL VL LC LW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P7M5'], [
        	'player'  		=> '7', 
        	'format'     	=> 'OR GA VL FO VL LC TR'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P8M1'], [
        	'player'  		=> '8', 
        	'format'     	=> 'SR GA FO VL VL VL LC SK'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P8M2'], [
        	'player'  		=> '8', 
        	'format'     	=> 'SR GA VL FO VL VL LC WW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P8M3'], [
        	'player'  		=> '8', 
        	'format'     	=> 'SR GA LV VL VL SK LC LW'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P8M4'], [
        	'player'  		=> '8', 
        	'format'     	=> 'SR GA VL FO MA MA LC SC'
        ]);
        DB::table('type')->updateOrInsert(['kdtype' => 'P8M5'], [
        	'player'  		=> '8', 
        	'format'     	=> 'SR GA PR VL TR VL LC LW'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P9M1'], [
        	'player'  		=> '9', 
        	'format'     	=> 'SR GA MA HN MA VL VL LC WW'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P10M1'], [
        	'player'  		=> '10', 
        	'format'     	=> 'SR GA VL HN TR MA MA WW LW SK'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P11M1'], [
        	'player'  		=> '11', 
        	'format'     	=> 'SR GA DR HN TR MA MA VL LC WW SK'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P12M1'], [
        	'player'  		=> '12', 
        	'format'     	=> 'SR GA DR HN VL MA MA VL LC WW SK TR'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P13M1'], [
        	'player'  		=> '13', 
        	'format'     	=> 'SR GA DR HN VL MA MA VL LC WW SK TR LV'
        ]);



        DB::table('type')->updateOrInsert(['kdtype' => 'P14M1'], [
        	'player'  		=> '14', 
        	'format'     	=> 'SR GA DR HN VL MA MA VL LC WW SK TR LV DG'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P15M1'], [
        	'player'  		=> '15', 
        	'format'     	=> 'SR GA DR HN VL MA MA VL LC WW SK TR LV DG VL'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P16M1'], [
        	'player'  		=> '16', 
        	'format'     	=> 'SR GA DR PR GN HN HA OR FO LV LC SC TR AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P17M1'], [
        	'player'  		=> '17', 
        	'format'     	=> 'SR GA DR PR GN HN HA OR FO WE LV LC SC TR AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P18M1'], [
        	'player'  		=> '18', 
        	'format'     	=> 'SR GA DR PR GN HN HA VL OR FO WE LV LC SC LW AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P19M1'], [
        	'player'  		=> '19', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA OR FO WE LV LC SC LW AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P20M1'], [
        	'player'  		=> '20', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA OR FO WE LV WW LC SC LW AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P21M1'], [
        	'player'  		=> '21', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA VL OR FO VL LV WW LC SC LW AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P22M1'], [
        	'player'  		=> '22', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA MA OR FO VL LV WW LC SC LW SK AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P23M1'], [
        	'player'  		=> '23', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA MA OR FO WE TR LV WW LC SC LW SK AR TN SN'
        ]);




        DB::table('type')->updateOrInsert(['kdtype' => 'P24M1'], [
        	'player'  		=> '24', 
        	'format'     	=> 'SR GA DR PR GN HN HA MA MA MA OR FO VL VL TR LV WW LC SC LW SK AR TN SN'
        ]);





    	// ROLE

        DB::table('role')->updateOrInsert(['kdrole' => 'AR'], [
        	'nama'  		=> 'Arsonist', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'R', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Tukang racun yang mbunuh2in orang'
        ]);


        DB::table('role')->updateOrInsert(['kdrole' => 'DG'], [
        	'nama'  		=> 'Dopelganger', 
        	'night'     	=> '0', 
        	'chance' 		=> '0', 
        	'first_night'   => '1', 
        	'team'    		=> 'X', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'Milih 1 orang, jika orang itu mati maka role orang itu pindah ke orang ini'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'DR'], [
        	'nama'  		=> 'Drunk', 
        	'night'     	=> '0', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Jika terbunuh, maka pembunuh akan kehilangan 1 malam'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'FO'], [
        	'nama'  		=> 'Seer', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'Bisa melihat role seseorang'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'GA'], [
        	'nama'  		=> 'Guardian Angel', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'Bisa melindungi seseorang setiap malam'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'GN'], [
        	'nama'  		=> 'Gunner', 
        	'night'     	=> '1', 
        	'chance' 		=> '2', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Vilager biasa yang bisa nembak orang 2 kali'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'HA'], [
        	'nama'  		=> 'Harlot', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'PL yang berkeliaran tiap malem'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'HN'], [
        	'nama'  		=> 'Hunter', 
        	'night'     	=> '0', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Vilager biasa yang jika terbunuh punya kesempatan untuk membunuh pembunuh (random)'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'LC'], [
        	'nama'  		=> 'Lycan', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'W', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Werewolf yang kalo dilihat seer hasilnya vilager'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'LV'], [
        	'nama'  		=> 'Lovers', 
        	'night'     	=> '0', 
        	'chance' 		=> '0', 
        	'first_night'   => '1', 
        	'team'    		=> 'X', 
        	'delta'    		=> '1',
        	'save'			=> '0',
        	'keterangan'	=> 'Milih 1 orang untuk jadi pasangan, mati bareng menang bareng'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'LW'], [
        	'nama'  		=> 'Lone Wolf', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'W', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'wolf yang kesepian, mencari temannya setiap malam. baru mau mbunuh mangsa kalau tinggal sendirian.'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'MA'], [
        	'nama'  		=> 'Mason', 
        	'night'     	=> '0', 
        	'chance' 		=> '0', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Sikembar yang tau betul siapa saudaranya'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'OR'], [
        	'nama'  		=> 'Oracle', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'Pemimpi yang dikasih tau bahwa seseorang adalah serigala atau bukan'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'PR'], [
        	'nama'  		=> 'Prince', 
        	'night'     	=> '0', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Pangeran yang menyamar jadi rakyat jelata, bisa lolos dari vote lych sekali'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'SC'], [
        	'nama'  		=> 'Sorcerrer', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'W', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Werewolf yang males mbunuh mangsa, kerjanya tiap malem nyari seer. Klo udah gak ada temen werewolf lagi (kecuali lone wolf) dia jadi suka mbunuh mangsa'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'SN'], [
        	'nama'  		=> 'Sand Man', 
        	'night'     	=> '1', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '1',
        	'save'			=> '0',
        	'keterangan'	=> 'Dengan kekuatannya dia bisa membuat aktifitas malam tertidur. pm: VOTE 0 ke GM'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'SK'], [
        	'nama'  		=> 'Serial Killer', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'S', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Jomblo Ngenes yang jadi pembunuh sadis'
        ]);


        DB::table('role')->updateOrInsert(['kdrole' => 'SR'], [
        	'nama'  		=> 'Seer', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '10',
        	'save'			=> '0',
        	'keterangan'	=> 'Bisa melihat role seseorang'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'TN'], [
        	'nama'  		=> 'Tanner', 
        	'night'     	=> '0', 
        	'chance' 		=> '0', 
        	'first_night'   => '0', 
        	'team'    		=> 'T', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Vilager biasa yang menang kalau di lych rame-rame'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'TR'], [
        	'nama'  		=> 'Traitor', 
        	'night'     	=> '0', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'W', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Vilager yang akan berubah jadi Werewolf jika semua werewolf mati'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'VL'], [
        	'nama'  		=> 'Villager', 
        	'night'     	=> '0', 
        	'chance' 		=> '0', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '0',
        	'keterangan'	=> 'Orang biasa untuk dikorbankan'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'WE'], [
        	'nama'  		=> 'Wise Elder', 
        	'night'     	=> '0', 
        	'chance' 		=> '1', 
        	'first_night'   => '0', 
        	'team'    		=> 'A', 
        	'delta'    		=> '0',
        	'save'			=> '1',
        	'keterangan'	=> 'bisa lolos dari percobaan pembunuhan sekali'
        ]);

        DB::table('role')->updateOrInsert(['kdrole' => 'WW'], [
        	'nama'  		=> 'Were Wolf', 
        	'night'     	=> '1', 
        	'chance' 		=> '999', 
        	'first_night'   => '0', 
        	'team'    		=> 'W', 
        	'delta'    		=> '100',
        	'save'			=> '0',
        	'keterangan'	=> 'Bisa makan mangsa tiap malam, jika 2 WereWolf atau lebih vote beda orang, maka yang mati random diantara pilihan tsb'
        ]);

        
    }
}
